#include "ObjSortingAlgorithmVisualizerUI.h"

#include <QTimer>
#include <iostream>
#include <list>

#include "./ui_ObjSortingAlgorithmVisualizer.h"

using namespace std;

ObjSortingAlgorithmVisualizerUI::ObjSortingAlgorithmVisualizerUI(
    QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::ObjSortingAlgorithmVisualizer)
{
  ui->setupUi(this);
}

ObjSortingAlgorithmVisualizerUI::~ObjSortingAlgorithmVisualizerUI() {
  delete ui;
}


