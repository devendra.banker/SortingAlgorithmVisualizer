﻿#include "ObjSortingAlgorithmVisualizerUI.h"
#include "gtest/gtest.h"

using namespace std;
using namespace testing;

TEST(ObjSortingAlgorithmVisualizerUITestSuite,
     CheckViewLoading) {
  ObjSortingAlgorithmVisualizerUI osavUI;
  osavUI.show();
  EXPECT_EQ(true, osavUI.isVisible());
}

TEST(ObjSortingAlgorithmVisualizerUITestSuite,
     WindowTitle) {
  ObjSortingAlgorithmVisualizerUI osavUI;
  EXPECT_EQ("SortingAlgorithmVisualizer",  osavUI.windowTitle().toStdString());
}



