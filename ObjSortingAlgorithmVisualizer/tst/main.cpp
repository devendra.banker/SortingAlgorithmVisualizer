#include <gtest/gtest.h>

#include <QApplication>
#include <QTimer>

int main(int argc, char *argv[]) {
  ::testing::InitGoogleTest(&argc, argv);
  QApplication a(argc, argv);

  QTimer exitTimer;
  QObject::connect(&exitTimer, &QTimer::timeout, &a, QApplication::quit);
  exitTimer.start();
  a.exec();

  return RUN_ALL_TESTS();
}
