﻿#ifndef OBJ_SORTING_ALGORITHM_VISUALIZER_UI_H
#define OBJ_SORTING_ALGORITHM_VISUALIZER_UI_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class ObjSortingAlgorithmVisualizer;
}

QT_END_NAMESPACE

class ObjSortingAlgorithmVisualizerUI : public QMainWindow {
  Q_OBJECT

 public:
  ObjSortingAlgorithmVisualizerUI(QWidget *parent = nullptr);
  ~ObjSortingAlgorithmVisualizerUI();

 private:
  Ui::ObjSortingAlgorithmVisualizer *ui;
};
#endif
