Introduction
------------
    - Sorting Algorithm Visualizer - Qt Based

Video Demonstration
-------------------
  - Sorting Library overview - f21208bad90b433fa7304a427f50671a6d21e34f Commit
      https://www.youtube.com/watch?v=_es7ITLHGKU&t=18s
  - Guess the name of the sorting algorithm
      https://www.youtube.com/watch?v=wImbGzvW068
  - Best Case | Bubble Sort | Animation | Sorting | Data Structures and Algorithms | Version 0.0.1 -
      https://www.youtube.com/watch?v=6WG1k6IT4a8&list=PLuqzaK4fClO8jTwwK4SgmmTxNzIxF8Lnh&index=1
  - Average Case | Bubble Sort | Animation | Sorting | Data Structures and Algorithms | Version 0.0.1
      https://www.youtube.com/watch?v=joPgMO8KT7g&list=PLuqzaK4fClO8jTwwK4SgmmTxNzIxF8Lnh&index=2
  - Worst Case | Bubble Sort | Animation | Sorting | Data Structures and Algorithms | Version 0.0.1
      https://www.youtube.com/watch?v=BZx36F8gX9U&list=PLuqzaK4fClO8jTwwK4SgmmTxNzIxF8Lnh&index=3



