#include <QApplication>

#include "ObjSortingAlgorithmVisualizerUI.h"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  ObjSortingAlgorithmVisualizerUI osav;
  osav.show();
  return a.exec();
}
